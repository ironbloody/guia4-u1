#include <fstream>
#include <iostream>
#include <string>
#include "Grafo.h"
using namespace std;

Grafo::Grafo() {}
  ofstream fp;

void Grafo::crear_imagen(Nodo *nodo){
  // Se abre el archivo.
  fp.open ("grafo.txt");
  
  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=yellow];" << endl;
  
  recorrer(nodo);
  
  fp << "}" << endl;
  
  // Se cierra el archivo.
  fp.close();
  
  // Se genera el grafo.
  system("dot -Tpng -ografo.png grafo.txt &");
  
  // Visualizacion del grafo.
  system("eog grafo.png &");
}
void Grafo::recorrer(Nodo *p) {
  if (p != NULL) {
    if (p->izq != NULL) {
      fp <<  p->info << "->" << p->izq->info << ";" << endl;
    } else{
      string cadena = std::to_string(p->info) + "i";
      fp << "\"" << cadena << "\"" << "[shape=point];" << endl;
      fp << p->info << "->" << "\"" << cadena << "\"" << ";" << endl;
    }
    if (p->der != NULL) { 
      fp << p->info << "->" << p->der->info << ";" << endl;
    } else{
      string cadena = std::to_string(p->info) + "d´";
      fp << "\"" << cadena << "\"" << "[shape=point];" << endl;
      fp << p->info << "->" << "\"" << cadena << "\"" << ";" << endl;
    }
      recorrer(p->izq);
      recorrer(p->der); 
  }
}
           