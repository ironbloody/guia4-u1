#include <fstream>
#include <iostream>
#include "Grafo.h"
using namespace std;

// Crea un nuevo nodo.
Nodo *crearNodo(int dato) {
  Nodo *q;
  q = new Nodo();
  q->izq = NULL;
  q->der = NULL;
  q->info = dato;
  return q;
}
// Encuentra el minimo.
Nodo* encontrarminimo(Nodo* nodo){
  // Temporal igualado a nodo;
  Nodo* temp = nodo;
  // Mientras el temporal y el temporal apuntando a izquierda sean distintos de nulos.
  while (temp && temp->izq != NULL){
    // temporal apuntara a izq.
    temp=temp->izq;
  }
  return temp;
}
// Funcion para eliminar nodos.
Nodo* eliminar(Nodo *nodo, int info){
  // Creacion variables
  Nodo* aux;
  Nodo* aux1;
  Nodo* temp;
  bool bo;
  
  // Si el nodo no tiene informacion retornara el nodo.
  if(nodo == NULL){
    return nodo;
  }
  // Si info es menor que el nodo apuntando a info.
  if(info < nodo->info){
      // Llama a la funcion pero con nodo apuntando a la izquierda.
      nodo->izq = eliminar(nodo->izq, info);
  }
  // Si info es mayor que el nodo apuntando a info.
  else if(info > nodo->info){
      // Llama a la funcion pero con nodo apuntando a la derecha.
       nodo->der = eliminar(nodo->der, info);
  }
  /* Si info es igual que el nodo apuntando a info, 
  entonces habra un nodo a eliminar*/
  else{
    // si el nodo apuntando a la derecha es nulo y 
    if(nodo->der == NULL && nodo->izq == NULL){
      // Se eliminara el nodo
      delete nodo;
      nodo = NULL;
    }
    
    // Si el nodo no tiene un hijo a la derecha.
    else if (nodo->der == NULL){
      // Se iguala el temporal.
      temp = nodo;
      // Nodo va ser igual al temporal apuntando a la izquierda.
      nodo = temp->izq;
      // Se elimina temporal.
      delete temp;
    }
    // Si el nodo no tiene un hijo a la izquierda.
    else if(nodo->izq == NULL){
      // Se iguala el temporal.
      temp = nodo;
      // Nodo va ser igual al temporal apuntando a la derecha.
      nodo = temp->der;
      // Se elimina temporal.
      delete temp;
    }
    // Si el nodo tiene 2 hijos.
    else{
      // Se encontrara el minimo del nodo.
      temp = encontrarminimo(nodo->der);
      // Nodo apuntando a info se iguala con temporal apuntando a info.
      nodo->info = temp->info;
      // Se eliminara el nodo.
      nodo->der = eliminar(nodo->der, temp->info);
        }
      }
  // Se retorna el nodo.
  return  nodo;
  }

// Funcion para insertar un nodo.
void insertarNodo(Nodo *nodo, int info){
  // Si info es mayor que el nodo aputando a info.
  if (info > nodo->info){
    // Si el nodo apuntando a derecha no tiene informacion.
    if(nodo->der == NULL){
      // Se creara un nodo a la derecha.
      nodo->der = crearNodo(info);
    } else {
      // Sino se llama a la funcion denuevo para volver a revisar.
      insertarNodo(nodo->der, info);
      }
    } else {
      // Si info es menor que el nodo apuntando a info.
      if (info < nodo->info){
      // Si el nodo apuntando a la izquierda no tiene informacion.
      if(nodo->izq == NULL){
        // Se creara un nodo a la izquieda.
        nodo->izq = crearNodo(info);
      } else {
        // Sino se llama a la funcion denuevo para volver a revisar.
        insertarNodo(nodo->izq, info);
    }
  }
  else{
      cout << "El numero ya existe" << endl;
    }
  }
}

// Funcion para modificar.
void modificar(Nodo *nodo, int info){
  // Creacion de variables.
  int numero;
  int nuevo;
  // Se ingresa el numero a modificar.
  cout << "Ingrese el numero a modificar"<< endl;
  // Se regista el numero.
  cin >> numero;
  // Se elimina el numero.
  eliminar(nodo, numero);
  // Se ingresa el nuevo numero.
  cout << "Ingrese el nuevo numero" << endl;
  // Se registra el nuevo numero.
  cin >> nuevo;
  // Se crea el nuevo numero en el arbol.
  insertarNodo(nodo, nuevo);
}

// Funcion que recorre el arbon en preorden.
void preorden(Nodo *nodo){
  if(nodo == NULL){
    return;
  }
  
  cout << nodo->info << " ";
  preorden(nodo->izq);
  preorden(nodo->der);
}

// Funcion que recorre el arbon en inorden.
void inorden(Nodo *nodo){
  if(nodo == NULL){
    return;
  }
  inorden(nodo->izq);
  cout << nodo->info << " ";
  inorden(nodo->der);
  
}

// Funcion que recorre el arbon en posorden.
void posorden(Nodo *nodo){
  if(nodo == NULL){
    return;
  }
  posorden(nodo->izq);
  posorden(nodo->der);
  cout << nodo->info << " ";
}


// Funcion main.
int main(void) {
    // Creacion de variables.
    Nodo n, *raiz = NULL;
    int opcion, numero, inicio, tamano;
    Grafo g = Grafo();
    // Se ingresa la raiz del arbol.
    cout << "Ingrese la raiz: " << endl;
    // Se registra y se crea.
    cin >> inicio;
    raiz = crearNodo(inicio);
    g.crear_imagen(raiz);
    
    // Menu.
    do {
      cout << "------------------------------------------------" << endl;
      cout << "1. Ingresar un numero" << endl;
      cout << "2. Eliminar numero" << endl;
      cout << "3. Modificar numero" << endl;
      cout << "4. Preorden " << endl;
      cout << "5. Inorden " << endl;
      cout << "6. Posorden" << endl;
      cout << "7. Salir" << endl;
      cout << "------------------------------------------------" << endl;
      
      // Ingreso de opcion.
      cout << "Ingrese la opcion: ";
      // Se guarda la opcion.
      cin >> opcion;
      
      switch (opcion) {
      
      case 1:{
          // Se ingresa el numero a agregar.
          cout << "Ingrese el numero" << endl;
          cin >> numero;
          // Se llama a la funcion insertarNodo para que inserte el nuevo numero
          insertarNodo(raiz, numero);
          g.crear_imagen(raiz);
        break;
      }
        
      case 2:{
        // Se ingresa el numero a eliminar.
        cout << "Ingrese el numero: " << endl;
        cin >> numero;
        // Se llama a la funcion eliminar para que elimine el numero.
        eliminar(raiz, numero);
        g.crear_imagen(raiz);
        break;
      }
        
      case 3:{
        // Se llama a modificar.
        modificar(raiz, numero);
        g.crear_imagen(raiz);
        break;
      }
      
      case 4:{
        // Se llama a preorden.
        preorden(raiz);
        cout << endl;
        break;
      }
      
      case 5:{
        // Se llama a inorden.
        inorden(raiz);
        cout << endl;
        break;
      }
      
      case 6:{
        // Se llama a posorden.
        posorden(raiz);
        cout << endl;
        break;
      }
        
      }
    }while (opcion != 7);
    return 0;
}

