**Guia 4 Unidad 1**                                                                                            
Programa basado en programación orientada a objetos y arboles binarios de busqueda.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                      
Ubuntu                                                                                                                                                                                     
graphviz                                                                                                                                                                           
**Instalación**                                                                                                                                       
-Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make.

-Instalar graphviz si no esta en su computador.

Para instalar graphviz inicie la terminal como super usuario y escriba lo siguiente:

apt-get install graphviz.


**Problematica**                                                                                                                                       
Crear un programa que logre hacer un arbol binario de busqueda que contenga solo numeros enteros, este debera poder eliminar, insertar o modificar numeros y ademas el usuario podra ver el arbol en preorden, inorden y posorden                             

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
AL abrir el programa este le pedira ingresar la raiz del arbol, una vez ingresada visualizara la imagen del arbol y un menu con 7 opciones.

La primera opcion le permite ingresar un numero al arbol y luego mostrara el arbol con el numero añadido, la segunda opcion le permite eliminar un numero del arbol, ordenandolo y mostrandole el arbol, la tercera le permite modificar un numero(eliminarlo y luego insertar otro), la cuarta opcion le mostrara el arbol en preorden, la quinta le mostrara el arbol en inorden, la sexta le mostrara el arbol en posorden y finalmente la septima opcion le permite salir del programa.

                                                                                            
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              
Fstream                                                                                                                                                  
String

**Versionado**                                                                                                                                        
Version 1.5                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               






