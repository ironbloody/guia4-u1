#include <fstream>
#include <iostream>
#include "Programa.h"
using namespace std;

class Grafo {
  private:
  public:  
    // Constructor.
    Grafo();
    // Funciones.
    void crear_imagen(Nodo *nodo);
    void recorrer(Nodo *p);
};